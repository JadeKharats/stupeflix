require "./spec_helper"

describe "Stupeflix" do
  it "renders /" do
    get "/"
    response.body.should eq "Hello World!"
  end
end
